pipeline {
  agent any

  options {
    disableConcurrentBuilds()
  }
  
  stages {
    stage('Restore') {
      steps {
        sh 'dotnet restore'
      }
    }

    stage('Test') {
      steps {
        dir('ConsoleApp') {
          sh 'dotnet test --logger "trx;LogFileName=TestOutput.trx"'
        }
      }
    }

    stage('Build & Sonar Analysis') {
      environment {
        scannerHome = tool 'SonarScanner-MSBuild'
      }
      steps {
        dir('ConsoleApp') {
          withSonarQubeEnv(installationName: 'sq1') {
            sh "dotnet ${scannerHome}/SonarScanner.MSBuild.dll begin /k:\"msbuild\" /n:\"MSBuild\""
            sh "dotnet build"
            sh "dotnet ${scannerHome}/SonarScanner.MSBuild.dll end"
          }
        }
      }
    }
  }

  post {
    success {
      script {
        BUILD_NAME = """${sh(returnStdout: true,script: "jq -r .FullSemVer ConsoleApp/obj/gitversion.json").trim()}"""
        currentBuild.displayName =  "#${BUILD_NAME}"
      }
    }
  }
}
